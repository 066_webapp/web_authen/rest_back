import { BillMaterial } from 'src/bill_materials/entities/bill_material.entity';
import { CheckMaterial } from 'src/check_materials/entities/check_material.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { MenuQueue } from 'src/menu_queues/entities/menu_queue.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { SalaryDetail } from 'src/salaries/entities/salary-detail';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  username: string;
  @Column()
  password: string;
  @Column()
  name: string;
  @Column()
  birthday: Date;
  @Column()
  address: string;
  @Column()
  tel: string;
  @Column()
  email: string;
  @Column()
  oth_contact: string;
  @Column()
  start_date: Date;
  @Column()
  role: string;
  @Column({ type: 'float' })
  sal_rate: number;
  @OneToMany(() => SalaryDetail, (salaryItem) => salaryItem.employeeId)
  salDetail: SalaryDetail;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => MenuQueue, (menuQueue) => menuQueue.menu)
  menuQueues: MenuQueue[];

  @OneToMany(() => BillMaterial, (billMaterials) => billMaterials.employee)
  billMaterials: BillMaterial[];

  @OneToMany(() => Receipt, (receipt) => receipt.employee)
  receipt: Receipt[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.employee)
  checkinout: Checkinout;

  @OneToMany(() => CheckMaterial, (checkMaterials) => checkMaterials.employee)
  checkMaterials: CheckMaterial[];
}
