import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckMaterialDto } from './dto/create-check_material.dto';
import { UpdateCheckMaterialDto } from './dto/update-check_material.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { Material } from 'src/materials/entities/material.entity';
import { CheckMaterial } from './entities/check_material.entity';
import { CheckMatDetail } from './entities/chmat_detail';

@Injectable()
export class CheckMaterialsService {
  constructor(
    @InjectRepository(CheckMaterial)
    private checkmatRepository: Repository<CheckMaterial>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(CheckMatDetail)
    private checkItemsRepository: Repository<CheckMatDetail>,
  ) {}

  async create(createCheckMaterialDto: CreateCheckMaterialDto) {
    console.log(createCheckMaterialDto.employeeId);
    const employee = await this.employeeRepository.findOneBy({
      id: createCheckMaterialDto.employeeId,
    });
    console.log(employee);
    const checkMat: CheckMaterial = new CheckMaterial();
    checkMat.employee = employee;
    await this.checkmatRepository.save(checkMat);
    for (const cd of createCheckMaterialDto.checkItems) {
      const checkItem = new CheckMatDetail();
      const materialf = await this.materialRepository.findOneBy({
        id: cd.materialId,
      });
      checkItem.material = materialf;
      checkItem.name = checkItem.material.name;
      checkItem.last_quantity = checkItem.material.quantity;
      checkItem.checkmat = checkMat;
      if (cd.quantity == -1) {
        checkItem.quantity = checkItem.material.quantity;
        materialf.quantity = checkItem.material.quantity;
      } else {
        checkItem.quantity = cd.quantity;
        materialf.quantity = cd.quantity;
      }
      this.materialRepository.update(materialf.id, materialf);
      await this.checkItemsRepository.save(checkItem);
    }
    await this.checkmatRepository.save(checkMat);
    return this.checkmatRepository.findOne({
      where: { id: checkMat.id },
      relations: ['checkItems'],
    });
  }

  findAll() {
    return this.checkmatRepository.find({
      relations: ['checkItems.material', 'employee'],
    });
  }

  async findOne(id: number) {
    const checkmat = await this.checkmatRepository.findOne({
      where: { id: id },
      relations: ['checkItems.material', 'employee'],
    });
    if (!checkmat) {
      throw new NotFoundException();
    }
    return checkmat;
  }

  async update(id: number, updateCheckMaterialDto: UpdateCheckMaterialDto) {
    const checkmat = await this.checkmatRepository.findOne({
      where: { id: id },
      relations: ['checkItems.material'],
    });
    if (!checkmat) {
      throw new NotFoundException();
    }
    if (updateCheckMaterialDto.employeeId != undefined) {
      checkmat.employee = await this.employeeRepository.findOneBy({
        id: updateCheckMaterialDto.employeeId,
      });
    }
    if (updateCheckMaterialDto.checkItems != undefined) {
      for (const ucd of updateCheckMaterialDto.checkItems) {
        const materialf = await this.materialRepository.findOneBy({
          id: ucd.materialId,
        });
        const cd = await this.checkItemsRepository.findOneBy({
          name: materialf.name,
          checkmat: { id: checkmat.id },
        });
        if (cd) {
          console.log(cd);
          cd.quantity = ucd.quantity;
          cd.checkmat = checkmat;
          this.checkItemsRepository.save(cd);
          materialf.quantity = cd.quantity;
          this.materialRepository.update(materialf.id, materialf);
        } else {
          const checkItem = new CheckMatDetail();
          checkItem.quantity = ucd.quantity;
          checkItem.material = materialf;
          checkItem.name = checkItem.material.name;
          checkItem.last_quantity = checkItem.material.quantity;
          checkItem.checkmat = checkmat;
          this.checkItemsRepository.save(checkItem);
          materialf.quantity += checkItem.quantity;
          this.materialRepository.update(materialf.id, materialf);
        }
      }
    }

    await this.checkmatRepository.save(checkmat);
    return this.checkmatRepository.findOne({
      where: { id: checkmat.id },
      relations: ['checkItems.material'],
    });
  }

  async remove(id: number) {
    const checkmat = await this.checkmatRepository.findOne({
      where: { id: id },
      relations: ['checkItems.material'],
    });
    if (!checkmat) {
      throw new NotFoundException();
    }
    for (const cd of checkmat.checkItems) {
      const materialf = await this.materialRepository.findOneBy({
        id: cd.material.id,
      });
      materialf.quantity = cd.last_quantity;
      this.materialRepository.update(materialf.id, materialf);
      await this.checkItemsRepository.softRemove(cd);
    }
    return this.checkmatRepository.softRemove(checkmat);
  }
}
