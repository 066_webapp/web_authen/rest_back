import { Module } from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { ReceiptsController } from './receipts.controller';
import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { ReceiptDetail } from './entities/receipt-detail';
import { Menu } from 'src/menus/entities/menu.entity';
@Module({
  imports: [
    TypeOrmModule.forFeature([Receipt, Table, Employee, ReceiptDetail, Menu]),
  ],
  controllers: [ReceiptsController],
  providers: [ReceiptsService],
})
export class ReceiptsModule {}
