import { Type } from 'class-transformer';
import {
  IsPositive,
  IsNotEmpty,
  IsInt,
  ValidateNested,
  Min,
} from 'class-validator';

class CreateBillDetailDto {
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  materialId: number;

  // @Min(1)
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  quantity: number;

  // @Min(5)
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  price: number;

  total: number;
}
export class CreateBillMaterialDto {
  @IsNotEmpty()
  shop_name: string;

  // @Min(5)
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  buy: number;

  total: number;

  // @IsInt()
  // @IsNotEmpty()
  change: number;

  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  employeeId: number;

  @Type(() => CreateBillDetailDto)
  @ValidateNested({ each: true })
  billItems: CreateBillDetailDto[];
}
