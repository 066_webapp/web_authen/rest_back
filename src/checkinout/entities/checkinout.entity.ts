import { Employee } from 'src/employees/entities/employee.entity';
import { SalaryDetail } from 'src/salaries/entities/salary-detail';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn()
  datetimeIn: Date;
  @Column({ nullable: true })
  datetimeOut: Date;
  @Column({ type: 'float' })
  total_hour: number;

  @ManyToOne(() => SalaryDetail, (salaryDetail) => salaryDetail.checkinout)
  salaryDetail: SalaryDetail;
  @ManyToOne(() => Employee, (employee) => employee.checkinout)
  employee: Employee;

  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;
}
