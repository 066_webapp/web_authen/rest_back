import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { Employee } from 'src/employees/entities/employee.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SalaryDetail } from 'src/salaries/entities/salary-detail';
import { Checkinout } from './entities/checkinout.entity';

@Injectable()
export class CheckinoutService {
  constructor(
    @InjectRepository(Employee)
    private EmployeeRepository: Repository<Employee>,
    @InjectRepository(SalaryDetail)
    private SaralyDetailRepository: Repository<SalaryDetail>,
    @InjectRepository(Checkinout)
    private CheckinoutRepository: Repository<Checkinout>,
  ) {}

  async create(createCheckinoutDto: CreateCheckinoutDto) {
    const cio = new Checkinout();
    console.log(createCheckinoutDto);
    cio.employee = await this.EmployeeRepository.findOneBy({
      id: createCheckinoutDto.employeeId,
    });
    if (!cio.employee) {
      throw new NotFoundException();
    }
    console.log(cio.datetimeOut);
    const cioc = await this.CheckinoutRepository.findOneBy({
      total_hour: 0,
      employee: { id: cio.employee.id },
    });
    console.log(cioc);
    if (cioc != null) {
      throw new Error();
    }
    console.log(cio.employee);
    cio.total_hour = 0;
    return this.CheckinoutRepository.save(cio);
  }

  findAll() {
    return this.CheckinoutRepository.find({
      relations: ['employee', 'salaryDetail'],
    });
  }

  async findOne(id: number) {
    const employeef = await this.EmployeeRepository.findOneBy({
      id: id,
    });
    console.log(employeef);
    const cio = await this.CheckinoutRepository.findOneBy({
      total_hour: 0,
      employee: { id: employeef.id },
    });
    if (!cio) {
      throw new NotFoundException();
    }
    console.log(cio);
    return cio;
  }

  async update(id: number, updateCheckinoutDto: UpdateCheckinoutDto) {
    console.log(updateCheckinoutDto);
    const cio = await this.CheckinoutRepository.findOne({
      where: { id: id },
      relations: ['employee', 'salaryDetail'],
    });
    if (!cio) {
      throw new NotFoundException();
    }
    if (updateCheckinoutDto.salaryDetailId != undefined) {
      cio.salaryDetail = await this.SaralyDetailRepository.findOneBy({
        id: updateCheckinoutDto.salaryDetailId,
      });
    }
    // cio.datetimeOut = updateCheckinoutDto.datetimeOut;
    if (updateCheckinoutDto.datetimeOut != undefined) {
      cio.datetimeOut = new Date();
      if (cio.datetimeIn.getUTCDate() == cio.datetimeOut.getUTCDate()) {
        const hour =
          (cio.datetimeOut.getUTCHours() * 60 +
            cio.datetimeOut.getUTCMinutes() -
            (cio.datetimeIn.getUTCHours() * 60 +
              cio.datetimeIn.getUTCMinutes())) /
          60;
        const parsehour = parseFloat(hour.toFixed(2));
        if (parsehour > 9) {
          cio.total_hour = 9;
        } else {
          cio.total_hour = parsehour;
        }
      }
      //console.log(cio.datetimeOut);
      //  const date = new Date();
      //console.log(date);
      return this.CheckinoutRepository.save(cio);
    } else {
      throw new Error();
    }
  }

  //don't use
  async remove(id: number) {
    return `This action deletes a #${id} checkinout`;
  }
}
