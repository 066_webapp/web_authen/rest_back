import { Module } from '@nestjs/common';
import { CheckinoutService } from './checkinout.service';
import { CheckinoutController } from './checkinout.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkinout } from './entities/checkinout.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { SalaryDetail } from 'src/salaries/entities/salary-detail';

@Module({
  imports: [TypeOrmModule.forFeature([Checkinout, Employee, SalaryDetail])],
  controllers: [CheckinoutController],
  providers: [CheckinoutService],
})
export class CheckinoutModule {}
