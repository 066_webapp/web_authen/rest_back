import { Injectable, NotFoundException } from '@nestjs/common';

import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { SalaryDetail } from './entities/salary-detail';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class SalariesService {
  constructor(
    @InjectRepository(Salary)
    private salariesRepository: Repository<Salary>,
    @InjectRepository(SalaryDetail)
    private salaryItemRepository: Repository<SalaryDetail>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectDataSource() private dataSource: DataSource,
  ) {}

  getYear() {
    return this.dataSource.query(
      `SELECT DISTINCT strftime('%Y', datetimeIn) as Year FROM checkinout`,
    );
  }
  async create(createSalaryDto: CreateSalaryDto) {
    const salary: Salary = new Salary();
    salary.date_start = createSalaryDto.date_start;
    salary.date_end = createSalaryDto.date_end;
    salary.date_salary = createSalaryDto.date_salary;
    salary.total = 0;
    await this.salariesRepository.save(salary);
    for (const sr of createSalaryDto.salaryItem) {
      const salaryItem = new SalaryDetail();
      salaryItem.employeeId = await this.employeeRepository.findOneBy({
        id: sr.empId,
      });
      salaryItem.emp_name = salaryItem.employeeId.name;
      salaryItem.emp_whours = sr.whours;
      salaryItem.emp_rate = salaryItem.employeeId.sal_rate;
      salaryItem.emp_total = salaryItem.emp_rate * salaryItem.emp_whours;
      salaryItem.salaryId = salary;
      await this.salaryItemRepository.save(salaryItem);
      salary.total += salaryItem.emp_total;
    }
    await this.salariesRepository.save(salary);
    return this.salariesRepository.findOne({
      where: { id: salary.id },
      relations: ['salaryDetailLists'],
    });
  }

  findAll() {
    return this.salariesRepository.find({ relations: ['salaryDetailLists'] });
  }

  async findOne(id: number) {
    const salary = await this.salariesRepository.findOne({
      where: { id: id },
      relations: ['salaryDetailLists'],
    });
    if (!salary) {
      throw new NotFoundException();
    }
    return salary;
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    const salary = await this.salariesRepository.findOneBy({ id: id });
    if (!salary) {
      throw new NotFoundException();
    }
    return this.salariesRepository.save({ ...salary, ...updateSalaryDto });
  }

  async remove(id: number) {
    const salary = await this.salariesRepository.findOne({
      where: { id: id },
      relations: ['salaryDetailLists'],
    });
    if (!salary) {
      throw new NotFoundException();
    }
    for (const sr of salary.salaryDetailLists) {
      await this.salaryItemRepository.softRemove(sr);
    }
    return this.salariesRepository.softRemove(salary);
  }
}
