import { Module } from '@nestjs/common';
import { SalariesService } from './salaries.service';
import { SalariesController } from './salaries.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Salary } from './entities/salary.entity';
import { SalaryDetail } from './entities/salary-detail';
import { Employee } from 'src/employees/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Salary, SalaryDetail, Employee])],
  controllers: [SalariesController],
  providers: [SalariesService],
})
export class SalariesModule {}
