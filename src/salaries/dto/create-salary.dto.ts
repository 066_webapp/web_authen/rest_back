import { Type } from 'class-transformer';
import { IsInt, IsNotEmpty, IsPositive, ValidateNested } from 'class-validator';
export class CreateSalaryDetailDto {
  @IsNotEmpty()
  @IsPositive()
  @IsInt()
  empId: number;
  @IsNotEmpty()
  whours: number;
  // @IsNotEmpty()
  // @IsPositive()
  // @IsInt()
  // empRate: number;
  // @IsNotEmpty()
  // @IsPositive()
  // @IsInt()
  // total: number;
}
export class CreateSalaryDto {
  @IsNotEmpty()
  date_start: Date;

  @IsNotEmpty()
  date_end: Date;

  @IsNotEmpty()
  date_salary: Date;

  @Type(() => CreateSalaryDetailDto)
  @ValidateNested({ each: true })
  salaryItem: CreateSalaryDetailDto[];

  // @IsNotEmpty()
  // @IsPositive()
  // @IsInt()
  // total: number;
}
