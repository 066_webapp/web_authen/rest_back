import { Employee } from './../employees/entities/employee.entity';
import { Menu } from './../menus/entities/menu.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { MenuQueuesService } from './menu_queues.service';
import { MenuQueuesController } from './menu_queues.controller';
import { MenuQueue } from './entities/menu_queue.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MenuQueue, Menu, Employee, Receipt])],
  controllers: [MenuQueuesController],
  providers: [MenuQueuesService],
})
export class MenuQueuesModule {}
