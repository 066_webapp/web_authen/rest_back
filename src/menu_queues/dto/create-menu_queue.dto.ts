import { IsInt, IsNotEmpty, IsPositive } from 'class-validator';

export class CreateMenuQueueDto {
  @IsNotEmpty()
  name: string;

  note: string;
  @IsNotEmpty()
  status: string;
  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  receiptId: number;
  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  menuId: number;

  chefId: number;
  waitressId: number;
}
