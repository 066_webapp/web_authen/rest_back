import { Employee } from 'src/employees/entities/employee.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Menu } from './../../menus/entities/menu.entity';

@Entity()
export class MenuQueue {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  note: string;
  @Column()
  status: string;
  @ManyToOne(() => Receipt, (receipt) => receipt.menuQueues)
  receipt: Receipt;
  // @Column() //{ nullable: true }
  // rec_id: number;
  @ManyToOne(() => Menu, (menu) => menu.menuQueues)
  menu: Menu;
  @ManyToOne(() => Employee, (chef) => chef.menuQueues)
  chef: Employee;
  @ManyToOne(() => Employee, (waitress) => waitress.menuQueues)
  waitress: Employee;

  @CreateDateColumn()
  createdAt: Date;
  @UpdateDateColumn()
  updatedAt: Date;
  @DeleteDateColumn()
  deletedAt: Date;
}
